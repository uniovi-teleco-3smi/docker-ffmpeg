# Contenedor con FFMPEG para la asignatura Servicios Multimedia e Interactivos

Clona el repositorio y modif�calo seg�n tus preferencias.

### Crear la imagen del contenedor

Para crear la imagen del contenedor sin iniciar el mismo:

```console
sudo docker-compose up --no-start
```

Para comprobar la creaci�n de la imagen:

```console
sudo docker images
```

Para comprobar que no se inicia el contenedor:

```console
sudo docker ps